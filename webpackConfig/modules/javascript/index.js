module.exports = {
  rules: [
    {
      test: /\.js$/,
      exclude: [/node_modules/],
      use: {
        loader: "babel-loader",
        options: {
          cacheDirectory: true,
          presets: ['@babel/preset-env'],
          plugins: ['syntax-dynamic-import']
        }
      }
    },
    {
      test: /[\/\\]node_modules[\/\\]some-module[\/\\]index\.js$/,
      use: "imports-loader?this=>window"
    }
  ]
};
