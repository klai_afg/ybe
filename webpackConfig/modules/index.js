const html = require('./html');
const javascript = require('./javascript');
const css = require('./css');
const fonts = require('./fonts');
const images = require('./images');
const vue = require('./vue');

module.exports = {
  htmlRules: html.rules,
  htmlPlugins: html.plugins,
  javascriptRules: javascript.rules,
  cssRules: css.rules,
  cssPlugins: css.plugins,
  fontsRules: fonts.rules,
  imgRules: images.rules,
  vueRules: vue.rules,
  vuePlugins: vue.plugins
};
