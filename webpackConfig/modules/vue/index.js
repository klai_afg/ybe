const {isVue} = require('@webpackConfig/index');

let rules = [];

let plugins = [];

if(isVue) {
  const {VueLoaderPlugin} = require('vue-loader');
  
  plugins.push(new VueLoaderPlugin());
  
  rules.push(
    {
      test: /\.vue$/,
      use: {
        loader: "vue-loader"
      }
    }
  )
}

module.exports = {rules, plugins};
