const {fontsParams} = require('@webpackConfig/index');


module.exports = {
  rules: [
    {
      test: /\.ttf|woff|woff2$/,
      loader: 'file-loader',
      options: {
        outputPath: fontsParams.output,
        context: fontsParams.context,
        name(file) {
          return '[path][name].[ext]';
        }
      }
    }
  ]
};
