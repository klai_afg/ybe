const path = require('path');

const {isBackend, htmlParams} = require('@webpackConfig/index');

const plugins = [];
const rules = [];

if(!isBackend) {
  const HtmlWebpackPlugin = require('html-webpack-plugin');
  
  plugins.push(
    new HtmlWebpackPlugin({
      template: path.resolve(htmlParams.src, 'pages/index.twig'),
      filename: 'index.html',
      chunks: ['bundle'],
      templateParameters: htmlParams.data
    })
  );
  
  rules.push(
    {
      test: /\.twig$/,
      exclude: [/node_modules/],
      use: [
        {
          loader: 'twig-loader'
        }
      ]
    }
  );
}

module.exports = {
  plugins,
  rules
};
