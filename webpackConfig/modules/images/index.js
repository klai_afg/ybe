const {isHMR, isProd} = require('@webpackConfig/settings');
const path = require('path');

const {imagesParams} = require('@webpackConfig/index');

const prodRules = [
  {
    loader: 'image-webpack-loader',
    options: {
      bypassOnDebug: true,
      svgo: {
        plugins: [
          {
            cleanupIDs: false,
            removeUselessDefs: false,
            removeUselessStrokeAndFill: false
          }
        ]
      },
    }
  }
];

const rules = [
  /*
  {
    test: /\.svg$/,
    include: path.resolve(dist.root, dist.svg),
    use: [
      {
        loader: "file-loader",
        options: {
          outputPath: path.join(paths.src.images),
          context: path.resolve(dist.root, dist.images),
          name(file) {
            return isHMR ? '[path][name].[hash].[ext]' : '[path][name].[ext]';
          }
        }
      }
    ]
  },
  */
  {
    test: /\.(jpg|png|svg|ico)$/,
    // exclude: path.resolve(dist.root, dist.svg),
    use: [
      {
        loader: "url-loader",
        options: {
          outputPath: imagesParams.output,
          context: imagesParams.context,
          limit: 10,
          name(file) {
            return '[path][name].[ext]';
          }
        },
      }
    ]
  }
];

if(isProd) {
  prodRules.forEach(rule => {
    rules.push(rule);
  });
}

module.exports = {
  rules
};
