const path = require('path');
const {isHMR, isProd} = require('@webpackConfig/settings');
const {isVue, cssParams, vueParams} = require('@webpackConfig/index');

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const autoprefixer = require('autoprefixer');

const commonUse = [
  {
    loader: "css-loader",
    options: {
      sourceMap: !isProd,
      importLoaders: 2
    }
  },
  {
    loader: "postcss-loader",
    options: {
      sourceMap: !isProd,
      plugins: [
        autoprefixer({
          browsers: ['ie >= 9', 'last 4 version']
        })
      ]
    }
  },
  {
    loader: "sass-loader",
    options: {
      sourceMap: !isProd,
      data: `@import "global";`,
      includePaths: [
        cssParams.src
      ]
    }
  }
];

const vueRules = [
  {
    test: /\.(sa|sc|c)ss$/,
    include: path.resolve(vueParams.src),
    use: [
      {
        loader: !isHMR ? MiniCssExtractPlugin.loader :  'vue-style-loader',
        options: !isHMR ? {
          publicPath: '../'
        } : {}
      },
      ...commonUse
    ]
  }
];

let rules = [
  {
    test: /\.(sa|sc|c)ss$/,
    exclude: path.resolve(vueParams.src),
    use: [
      {
        loader: !isHMR ? MiniCssExtractPlugin.loader : 'style-loader',
        options: !isHMR ? {
          publicPath: '../'
        } : {}
      },
      ...commonUse
    ]
  }
];

if(isVue) {
  rules = rules.concat(vueRules);
}


const plugins = [
  new MiniCssExtractPlugin({
    filename:  path.join(cssParams.output, isHMR ? '[name].[hash].css' : '[name].css'),
    chunkFilename: path.join(cssParams.output, isHMR ? '[name].[hash].css' : '[name].css'),
    // allChunks: false
  })
];

if(isProd) {
  const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
  
  const cssOptimize = new OptimizeCssAssetsPlugin({
    assetNameRegExp: /\.css$/g,
    cssProcessor: require('cssnano'),
    cssProcessorPluginOptions: {
      preset: ['default', { discardComments: { removeAll: true } }],
    },
    canPrint: true
  });
  
  plugins.push(cssOptimize);
}

module.exports = {
  rules,
  plugins
};
