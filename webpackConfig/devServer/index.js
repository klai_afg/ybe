const path = require('path');
const {hostName, isHMR} = require('@webpackConfig/settings');
const {isBackend} = require('@webpackConfig/index');

let options;

if (isBackend) {
  options = {
    clientLogLevel: 'none',
    index: 'index.html',
    contentBase: false,
    proxy: {
      '/': {
        target: hostName,
        changeOrigin: true,
        secure: false
      }
    }
  };
} else {
  options = {
    open: true,
    clientLogLevel: 'none',
    index: 'index.html',
    contentBase: false,
    historyApiFallback: true,
    disableHostCheck: true
  };
}

let plugins = [];

if (isBackend) {
  const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
  plugins.push(new BrowserSyncPlugin(
    {
      proxy: isHMR ? 'http://localhost:8080' : hostName,
      logLevel: 'silent',
      ghostMode: false,
      files: [
        {
          match: [
            'themes/resources/pages/*.htm',
            'themes/resources/partials/**/*.htm'
          ],
          fn: function (event, file) {
            if (event === 'change') {
              const bs = require('browser-sync').get('bs-webpack-plugin');
              bs.reload();
              console.log(file + ' ' + event);
            }
          }
        }
      ]
    },
    {
      reload: false
    }));
}

module.exports = {options, plugins};
