const isBackend = true;
const isVue = false;
const path = require('path');

let htmlParams;

const pathRoot = {
  dist: 'themes/resources',
  distAssets: 'themes/resources/assets_dev',
  src: 'themes/resources/assets',
  srcAssets: '' //Relative to src
};

if(!isBackend) {
  htmlParams = {
    src: path.join(pathRoot.dist, 'views'),
    data: {
      all: require(path.resolve(pathRoot.dist, 'data/all.json'))
    }
  };
}



const jsParams = {
  src: path.join(pathRoot.dist, 'js'),
  output: path.join('js')
};

const cssParams = {
  src: path.resolve(pathRoot.dist, 'sass'),
  output: path.join('css'),
  extraOutput: path.resolve(pathRoot.src, 'css/extra')
};

const imagesParams = {
  src: path.join(pathRoot.distAssets, 'images'),
  context: path.join(pathRoot.distAssets, 'images'),
  output: path.join(pathRoot.srcAssets, 'images')
};

const fontsParams = {
  src: path.join(pathRoot.distAssets ,'fonts'),
  context: path.join(pathRoot.distAssets ,'fonts'),
  output: path.join(pathRoot.srcAssets, 'fonts'),
};

const vueParams = {
  src: path.join(pathRoot.dist, 'vue')
};

const svgSpriteParams = {
  src: path.join(pathRoot.distAssets, 'svg'),
  variables: path.join(pathRoot.dist, 'sass/common/_svg.scss'),
  template: path.join(pathRoot.dist, 'sass/common/svg/_template.scss'),
  output: path.resolve(pathRoot.distAssets, 'images/sprite')
};


const pathsClean = [
  path.join(pathRoot.src, 'images'),
  path.join(pathRoot.src, 'fonts'),
  path.join(pathRoot.src, 'js'),
  path.join(pathRoot.src, 'css'),
];

//resources/sass/[fileName]
const extractCssFiles = [];

module.exports = {
  pathRoot,
  isVue,
  isBackend,
  extractCssFiles,
  fontsParams,
  imagesParams,
  svgSpriteParams,
  cssParams,
  htmlParams,
  vueParams,
  jsParams,
  pathsClean
};