const path = require('path');
const isHMR = process.argv.includes('--hot');
const NODE_ENV = process.env.NODE_ENV || 'development';
const isProd = NODE_ENV === 'production';
let hostName;

const {isVue, imagesParams, fontsParams, jsParams, vueParams} = require('./index');

try {
  hostName = require('./proxy');
} catch (ex) {
  hostName = null;
}


let aliases = {
  'jquery': path.join(process.cwd(), 'node_modules/jquery/dist/jquery'),
  images: path.resolve(imagesParams.src),
  fonts: path.resolve(fontsParams.src),
  'bootstrap-custom': path.resolve(jsParams.src, 'bootstrap'),
  components: path.resolve(jsParams.src, 'components'),
  modules: path.resolve(jsParams.src, 'modules'),
  plugins: path.resolve(jsParams.src, 'plugins'),
  utils: path.resolve(jsParams.src, 'utils')
};

if (isVue) {
  const vueAliases = {
    'vue-bootstrap': path.resolve(vueParams.src, 'bootstrap'),
    'vue-common': path.resolve(vueParams.src, 'common'),
    'vue-components': path.resolve(vueParams.src, 'components'),
    'vue-mixins': path.resolve(vueParams.src, 'mixins'),
    'vue-modules': path.resolve(vueParams.src, 'modules'),
    'vue-utils': path.resolve(vueParams.src, 'utils'),
    'vue$': 'vue/dist/vue.esm.js'
  };
  
  aliases = Object.assign({}, aliases, vueAliases);
}

module.exports = {
  isHMR,
  NODE_ENV,
  isProd,
  aliases,
  hostName,
};
