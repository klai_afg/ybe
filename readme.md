##Init project


1. Create database in phpMyAdmin
    * collation "utf8_general_ci"
    * import database with "october_sample_base.sql"
2. Create database.php in '/config'
    * create a file ```/config/database.php``` based on ```/config/_database_sample.php```
    * change in created file this fields: mysql.database (from step 2), mysql.username (usually 'root'), mysql.password (usually 'root')
    * set mysql.port (usually 3306 or 8889)
    * set mysql.host (usually 127.0.0.1 or localhost)
3. Run in terminal ```$ npm\yarn install```
4. Run in terminal ```$ php artisan october:up```
5. Set current WebpackProxy server
    * create a file ```/webpackConfig/proxy.js``` based on ```/webpackConfig/proxy_sample.js```
    * instead of ```http://hostName:8888``` set up your host
6. Run in terminal ```$ npm\yarn run watch```


## Work with backend
##### admin panel:
hostname/backend

##### dev user:
- user: admin
- pass: q1w22e333

## Build project

#### run in terminal

hmr

* ```$ npm\yarn run hot```
* ```$ npm\yarn run hot:host```

development

* ```$ npm\yarn run build:dev```

for public 

* ```$ npm\yarn run build:prod```