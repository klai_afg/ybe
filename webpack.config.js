require('module-alias/register');
const webpack = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const path = require('path');
const {NODE_ENV, aliases} = require('./webpackConfig/settings');
const {isBackend, jsParams, pathRoot, pathsClean} = require('./webpackConfig/index');
const TerserPlugin = require('terser-webpack-plugin');

/* ---------------------------------Require Modules------------------------------------ */

const {
  htmlRules,
  htmlPlugins,
  javascriptRules,
  cssRules,
  cssPlugins,
  fontsRules,
  imgRules,
  vuePlugins,
  vueRules
} = require('./webpackConfig/modules');


/* ---------------------------------Require devServer plugins & options------------------------------------ */

const {
  options: devServerOptions,
  plugins: devServerPlugins
} = require('./webpackConfig/devServer');

const webpackConfig = {
  entry: {
    bundle: ['@babel/polyfill', path.resolve(jsParams.src, 'app.js')],
  },
  output: {
    path: path.resolve(__dirname, pathRoot.src),
    chunkFilename: path.join(jsParams.output, '[name].bundle.js'),
    filename: path.join(jsParams.output, '[name].js'),
    publicPath: isBackend ? '/' + pathRoot.src + '/' : '/'
  },
  devtool: 'cheap-module-source-map',
  devServer: devServerOptions,
  performance: {
    hints: false
  },
  resolve: {
    alias: aliases,
    extensions: ['.js', '.vue', '.json']
  },
  optimization: {
    minimizer: [new TerserPlugin()],
    splitChunks: {
      cacheGroups: {
        vendor: {
          name: 'vendor',
          test: /[\\/]node_modules[\\/]/,
          chunks: 'all',
        }
      }
    }
  },
  module: {
    rules: [
      /* ---------------------------------Html------------------------------------ */
      ...htmlRules,
      /* ---------------------------------JS------------------------------------ */
      ...javascriptRules,
      /* ---------------------------------CSS------------------------------------ */
      ...cssRules,
      /* ---------------------------------Fonts------------------------------------ */
      ...fontsRules,
      /* ---------------------------------Images------------------------------------ */
      ...imgRules,
      /* ---------------------------------Vue------------------------------------ */
      ...vueRules
    ]
  },
  plugins: [
    /* ---------------------------------Html------------------------------------ */
    ...htmlPlugins,
    /* ---------------------------------CSS------------------------------------ */
    ...cssPlugins,
    /* ---------------------------------Define------------------------------------ */
    new webpack.DefinePlugin({
      NODE_ENV: JSON.stringify(NODE_ENV)
    }),
    /* ---------------------------------Clean------------------------------------ */
    new CleanWebpackPlugin(pathsClean, {
      verbose: true
    }),
    /* ---------------------------------Vue------------------------------------ */
    ...vuePlugins,
    ...devServerPlugins
  ]
};

module.exports = webpackConfig;
