//GULP
const path = require('path');
const gulp = require('gulp');
const watch = require('gulp-watch');
const svgSprite = require('gulp-svg-sprite');
const fs = require('fs');
const cheerio = require('gulp-cheerio');
const replace = require('gulp-replace');
const rename = require('gulp-rename');
const clean = require('gulp-clean');
const gulpif = require('gulp-if');

const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const cssnano = require('gulp-cssnano');
const sass = require('gulp-sass');
sass.compiler = require('node-sass');

const {extractCssFiles, svgSpriteParams, cssParams, pathRoot} = require('./webpackConfig/index');

const isProd = process.env.NODE_ENV === 'production';
/* ---------------------------------CSS------------------------------------ */

gulp.task('sass', function () {
  return gulp.src([
    ...extractCssFiles.map(fileName => path.resolve(cssParams.src, fileName))
  ])
    .pipe(replace('~', `/${pathRoot.src}/`))
    .pipe(gulpif(!isProd, sourcemaps.init()))
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: ['ie >= 9', 'last 4 version']
    }))
    .pipe(gulpif(isProd, cssnano()))
    .pipe(gulpif(!isProd, sourcemaps.write()))
    .pipe(gulp.dest(cssParams.extraOutput));
});

gulp.task('sass:watch', function () {
  watch([
    ...extractCssFiles.map(fileName => path.resolve(cssParams.src, fileName))
  ], function () {
    gulp.start('sass');
  });
});

/* ---------------------------------SvgSprite------------------------------------ */

const svgSpriteOptions = svgSpriteParams;

//Sprite svg
gulp.task('cleanSvgSingleParsed', function () {
  return gulp.src(path.resolve(svgSpriteOptions.src, 'single_parsed'), {read: false})
    .pipe(clean());
});

gulp.task('svgParse', ['cleanSvgSingleParsed'], function () {
  return gulp.src(path.resolve(svgSpriteOptions.src, 'single/**/*.svg'))
    .pipe(cheerio({
      run: function ($) {
        
        let $fill = $('[fill]');
        let $stroke = $('[stroke]');
        
        $stroke.each(function () {
          let $currentStroke = $(this);
          
          if($currentStroke.attr('stroke') !== 'none') {
            $currentStroke.attr('stroke', 'currentColor');
          }
        });
        
        $fill.each(function () {
          let $currentFill = $(this);
          
          if($currentFill.attr('fill') !== 'none') {
            $currentFill.attr('fill', 'currentColor');
          }
        });
        
        
        $('[style]').removeAttr('style');
      },
      parserOptions: {xmlMode: true}
    }))
    .pipe(rename(function (path) {
      path.basename = 's-' + path.basename;
    }))
    .pipe(gulp.dest(path.resolve(svgSpriteOptions.src, 'single_parsed')));
});

const svgSymbolMode = {
  symbol: {
    dest: "./",
    sprite: svgSpriteOptions.output,
    bust: false,
    render: {
      scss: {
        dest: svgSpriteOptions.variables,
        template: svgSpriteOptions.template
      }
    }
  }
};


gulp.task('svg:sprite', ['svgParse'], function () {
  return gulp.src([
    path.resolve(svgSpriteOptions.src, 'single_parsed/**/*.svg'),
    path.resolve(svgSpriteOptions.src, 'multi/**/*.svg')
  ])
    .pipe(replace('&gt;', '>'))
    .pipe(svgSprite({
      mode: svgSymbolMode,
    }))
    .pipe(gulp.dest('./'));
});


gulp.task('svg:watch', function () {
  watch([
    path.resolve(svgSpriteOptions.src, 'single/**/*.svg'),
    path.resolve(svgSpriteOptions.src, 'multi/**/*.svg')
  ], function () {
    gulp.start('svg:sprite');
  });
});

gulp.task('default', ['sass', 'svg:sprite']);

gulp.task('watch', ['svg:sprite', 'svg:watch', 'sass', 'sass:watch']);
