import {arrayByClass, addClass, removeClass} from "utils/dom";

const eventSelect = new CustomEvent('selectaps.select');

export class Selectaps {
  constructor(elem, params = {}) {

    addClass(elem.querySelector('.js-selectaps'), '_init');

    const initOptions = {

    };

    this.elem = elem;
    this.elDropdown = elem.querySelector('.js-selectaps-dropdown');
    this.elSelection = elem.querySelector('.js-selectaps-selection');
    this.initTitle = this.elSelection.querySelector('.js-selectaps-selection-title').innerText;
    this.elSelect = elem.querySelector('select');
    this.options = {...initOptions, ...params};
    this.elem.selectaps = this;

    this.bindEvents();
  }

  close() {
    this.elDropdown.setAttribute('data-open', 'false');
  }

  open() {
    this.elDropdown.setAttribute('data-open', 'true');
  }

  toggle() {
    if (this.elDropdown.getAttribute('data-open') === 'true') this.close(); else this.open();
  }

  select(item) {
    const value = item.getAttribute('data-value');
    const title = item.innerText;
    this.close();
    this.elSelect.value = value;
    this.value = value;
    const prevSelected = this.elDropdown.querySelector('.js-selectaps-item.is-selected');
    prevSelected && removeClass(prevSelected,'is-selected');
    addClass(item, 'is-selected');
    this.elSelection.querySelector('.js-selectaps-selection-title').innerHTML = title;

    this.elem.dispatchEvent(eventSelect);
  }

  clear() {
    this.elSelect.value = '';
    const prevSelected = this.elDropdown.querySelector('.js-selectaps-item.is-selected');
    prevSelected && removeClass(prevSelected,'is-selected');
  }

  bindEvents() {
    this.elSelection.addEventListener('click', () => this.toggle());

    this.elDropdown.addEventListener('click', (e) => {
      let dropdownItem = e.target.closest('.js-selectaps-item');
      if (!dropdownItem || !this.elDropdown.contains(dropdownItem)) return;
      this.select(dropdownItem);
    });

    document.addEventListener('click', (e) => {
      if (this.elem.contains(e.target)) return;
      this.close();
    })
  }

  on(event, func) {
    this.elem.addEventListener('selectaps.' + event, () => func(this.value));
  }
}