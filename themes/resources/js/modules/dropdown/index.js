import { arrayByClass, removeClass, addClass } from 'utils/dom'

export class WeberryDropDown {
  constructor (elem, params) {
    
    if(elem.WeberryDropDown) {
      return elem.WeberryDropDown;
    }
    
    const defaultOptions = {
      toggleClass: 'js-dropdown-toggle',
      toggleData: 'data-open',
      menuOpenClass: 'is-open',
      menuAnimateClass: 'is-animate',
      menuClass: 'js-dropdown-menu',
      menuAnimateInDelay: 1,
      menuAnimateOutDelay: 300,
      onShow() {},
      onHide() {},
      onMenuAnimateIn(){}
    }
  
    const options = Object.assign({}, defaultOptions, params)
    
    this.elem = elem;
    this._defaultOptions = defaultOptions;
    this.options = options;
    this.isOpen = false;
    this._menuAnimateShowTimeout = null;
    this._menuAnimateHideTimeout = null;
  
  
    this.toggles = arrayByClass(elem, options['toggleClass']);
    this.menu = arrayByClass(elem, options['menuClass'], true);
  
  
    elem.WeberryDropDown = this;
  
  
    this.init();
  }
  
  setOption(option, value) {
    this.options[option] = value;
  }
  
  show() {
    const {toggles, menu, options, elem} = this;
    const toggleData = options.toggleData;
  
    clearTimeout(this._menuAnimateShowTimeout);
    clearTimeout(this._menuAnimateHideTimeout);
  
    addClass(menu, options['menuOpenClass']);
  
    toggles.forEach(toggle => {
      toggle.setAttribute(toggleData, 'true')
    })
  
    options.onShow(elem);
    
  
    this._menuAnimateShowTimeout = setTimeout(() => {
  
      options.onMenuAnimateIn(elem);
      addClass(menu, options['menuAnimateClass']);
      
    }, options.menuAnimateInDelay);
    
    this.isOpen = true;
  }
  
  hide() {
    const {toggles, menu, options, elem} = this;
    const toggleData = options.toggleData;
  
    clearTimeout(this._menuAnimateShowTimeout);
    clearTimeout(this._menuAnimateHideTimeout);
  
    removeClass(menu, options['menuAnimateClass']);
  
    toggles.forEach(toggle => {
      toggle.setAttribute(toggleData, 'false')
    })
    
    this._menuAnimateHideTimeout = setTimeout(() => {
      
      options.onHide(elem);
      
      removeClass(menu, options['menuOpenClass']);
    }, options.menuAnimateOutDelay)
  
    this.isOpen = false;
  }
  
  toggle() {
    this.isOpen ? this.hide() : this.show();
  }
  
  bindEvents() {
    const {_closeListener, _openListener, toggles} = this;
    
    document.addEventListener('click', _closeListener)
  
    toggles.forEach(toggleDom => {
      toggleDom.addEventListener('click', _openListener)
    })
  }
  
  unbindEvents() {
    const {_closeListener, _openListener, toggles} = this;
  
    document.removeEventListener('click', _closeListener);
    toggles.forEach(toggleDom => {
      toggleDom.removeEventListener('click', _openListener)
    })
  }
  
  init() {
    const _self = this;
    
    const {elem} = this;
  
    const openListener = (e) => {
      e.preventDefault();
      _self.toggle();
    }
    
    const closeListener = (e) => {
      if (!elem.contains(e.target)) {
        _self.hide()
      }
    }
    
    
    this._closeListener = closeListener;
    this._openListener = openListener;
    
    
    this.bindEvents();
  }
  
  destroy() {
  
    const {options} = this;
    
    this.unbindEvents();
    
    clearTimeout(this._menuAnimateShowTimeout);
    clearTimeout(this._menuAnimateHideTimeout);
    
    removeClass(this.menu, options.menuAnimateClass);
    removeClass(this.menu, options.menuOpenClass);
    
    this.toggles.forEach(toggle => {
      toggle.removeAttribute(this.options.toggleData);
    })
    
    this.elem.WeberryDropDown = null;
  }
}