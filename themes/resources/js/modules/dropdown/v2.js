import { arrayByClass, removeClass, addClass } from 'utils/dom';

export class WeberryDropDown2 {
  constructor (elem, params) {
    
    if (elem.WeberryDropDown) {
      return elem.WeberryDropDown;
    }
    
    let globalDropdowns;
    
    if(window.weberryDropdowns) {
      globalDropdowns = window.weberryDropdowns;
    }
    
    let id;
    
    if(globalDropdowns) {
      const keys = Object.keys(globalDropdowns);
      const lastKey = keys[keys.length - 1];
      id = parseInt(lastKey) + 1;
      window.weberryDropdowns[id] = elem;
    } else {
      window.weberryDropdowns = {};
      id = 1;
    }
    
    window.weberryDropdowns[id] = elem;
    
    const defaultOptions = {
      toggleClass: 'js-dropdown-toggle',
      toggleData: 'data-open',
      toggleClick: true,
      toggleHover: true,
      menuOpenClass: 'is-open',
      menuAnimateClass: 'is-animate',
      menuClass: 'js-dropdown-menu',
      menuAnimateInDelay: 1,
      menuAnimateOutDelay: 300,
      onShow () {},
      onHide () {},
      onMenuAnimateIn () {}
    };
    
    const options = Object.assign({}, defaultOptions, params);
    
    this.id = id;
    this.elem = elem;
    this._defaultOptions = defaultOptions;
    this.options = options;
    this.isOpen = false;
    this.listeners = {};
    
    this._menuAnimateShowTimeout = null;
    this._menuAnimateHideTimeout = null;
    this._hideTimeout = null;
    
    this.toggles = arrayByClass(elem, options['toggleClass']);
    this.menu = arrayByClass(elem, options['menuClass'], true);
    
    elem.WeberryDropDown = this;
    
    this.init();
  }
  
  setOption (option, value) {
    this.options[option] = value;
  }
  
  show () {
    const {toggles, menu, options, elem, isOpen} = this;
    const toggleData = options.toggleData;
    
    clearTimeout(this._hideTimeout);
    
    if(isOpen) {
      return false;
    }
    
    if(window.weberryDropdowns) {
      for(let dropdownId in window.weberryDropdowns) {
        if(this.id !== dropdownId) {
          window.weberryDropdowns[dropdownId].WeberryDropDown.hide();
        }
      }
    }
    
    clearTimeout(this._menuAnimateShowTimeout);
    clearTimeout(this._menuAnimateHideTimeout);
    
    addClass(menu, options['menuOpenClass']);
    
    toggles.forEach(toggle => {
      toggle.setAttribute(toggleData, 'true');
    });
    
    options.onShow(this, elem);
    
    this._menuAnimateShowTimeout = setTimeout(() => {
      
      options.onMenuAnimateIn(elem);
      addClass(menu, options['menuAnimateClass']);
      
    }, options.menuAnimateInDelay);
    
    this.isOpen = true;
  }
  
  hide () {
    const {toggles, menu, options, elem, isOpen} = this;
    const toggleData = options.toggleData;
    
    if(!isOpen) {
      return false;
    }
    
    clearTimeout(this._menuAnimateShowTimeout);
    clearTimeout(this._menuAnimateHideTimeout);
    
    removeClass(menu, options['menuAnimateClass']);
    
    toggles.forEach(toggle => {
      toggle.setAttribute(toggleData, 'false');
    });
    
    this._menuAnimateHideTimeout = setTimeout(() => {
      
      options.onHide(elem);
      
      removeClass(menu, options['menuOpenClass']);
    }, options.menuAnimateOutDelay);
    
    this.isOpen = false;
  }
  
  toggle () {
    this.isOpen ? this.hide() : this.show();
  }
  
  bindEvents () {
    const {listeners, toggles, menu} = this;
    
    document.addEventListener('click', listeners.documentClose);
    
    toggles.forEach(toggleDom => {
      toggleDom.addEventListener('click', listeners.toggle);
      
      toggleDom.addEventListener('mouseenter', listeners.open);
      toggleDom.addEventListener('mouseleave', listeners.closeDelay);
    });
    
    menu.addEventListener('mouseenter', listeners.open);
    menu.addEventListener('mouseleave', listeners.closeDelay);
  }
  
  unbindEvents () {
    const {listeners, toggles, menu} = this;
    
    document.removeEventListener('click', listeners.documentClose);
    
    toggles.forEach(toggleDom => {
      toggleDom.removeEventListener('click', listeners.toggle);
      
      toggleDom.removeEventListener('mouseenter', listeners.open);
      toggleDom.removeEventListener('mouseleave', listeners.closeDelay);
    });
    
    menu.removeEventListener('mouseenter', listeners.open);
    menu.removeEventListener('mouseleave', listeners.closeDelay);
  }
  
  createListeners() {
    const _self = this;
    
    const {elem} = this;
    
    const documentCloseListener = (e) => {
      
      if (!elem.contains(e.target)) {
        _self.hide();
      }
    };
    
    const openListener = (e) => {
      e.preventDefault();
      this.show();
    };
    
    const closeListener = (e) => {
      e.preventDefault();
      this.hide();
    };
    
    const toggleListener = (e) => {
      e.preventDefault();
      this.toggle();
    };
    
    const closeDelayListener = () => {
      _self._hideTimeout = setTimeout(() => {
        this.hide();
      }, 500);
    };
    
    this.listeners = {
      closeDelay: closeDelayListener,
      close: closeListener,
      open: openListener,
      toggle: toggleListener,
      documentClose: documentCloseListener
    };
    
  }
  
  init () {
    this.createListeners();
    this.bindEvents();
  }
  
  destroy () {
    
    const {options} = this;
    
    this.unbindEvents();
    
    clearTimeout(this._menuAnimateShowTimeout);
    clearTimeout(this._menuAnimateHideTimeout);
    clearTimeout(this._hideTimeout);
    
    removeClass(this.menu, options.menuAnimateClass);
    removeClass(this.menu, options.menuOpenClass);
    
    this.toggles.forEach(toggle => {
      toggle.removeAttribute(this.options.toggleData);
    });
    
    
    delete this.elem.WeberryDropDown;
    delete window.weberryDropdowns[this.id];
  }
}