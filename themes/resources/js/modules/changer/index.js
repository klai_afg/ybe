import {arrayByClass, addClass, hasClass, removeClass} from "utils/dom";
import anime from 'animejs/lib/anime.es.js';

const eventPrev = new CustomEvent('changer.prev');
const eventNext = new CustomEvent('changer.next');
const eventChange = new CustomEvent('changer.change');

export class Changer {
  constructor(elem, params = {}) {
    // addClass(elem.querySelector('.js-selectaps'), '_init');

    const initOptions = {
      speed: 400,
      initialSlide: 0,
      matcher: (a, b) => a === b
    };

    this.elem = elem;
    this.elPrev = elem.querySelector('.js-changer-prev');
    this.elNext = elem.querySelector('.js-changer-next');
    this.elList = elem.querySelector('.js-changer-list');
    this.list = arrayByClass(this.elList, 'js-changer-item-wr');
    this.listLength = this.list.length;
    this.elContent = elem.querySelector('.js-changer-content');
    this.withInput = hasClass(this.elContent, '_with-input');
    this.elInput = elem.querySelector('.js-changer-input');
    this.slideSize = this.list.length ? this.list[0].getBoundingClientRect().width : 0;

    if (this.withInput) {
      this.elListWr = elem.querySelector('.js-changer-list-wr');
    }

    this.options = {...initOptions, ...params};

    this.bindEvents();

    this.to(this.options.initialSlide);
  }

  prev() {
    this.to(this.currentSlide ? this.currentSlide - 1 : this.listLength - 1, eventPrev);
  }

  next() {
    this.to((this.currentSlide + 1) % this.listLength, eventNext);
  }

  to(num, event, duration = this.options.speed) {
    this.currentSlide = num;
    this.elInput.value = this.list[this.currentSlide].innerText;
    anime({
      targets: this.elList,
      duration: duration,
      translateX: -1 * num * this.slideSize,
      easing: 'easeInQuad',
      complete: () => {
        this.elem.dispatchEvent(eventChange);
        event && this.elem.dispatchEvent(event);
      }
    });
  }

  bindEvents() {
    this.elPrev.addEventListener('click', () => this.prev());
    this.elNext.addEventListener('click', () => this.next());
    if (this.withInput) {
      this.elContent.addEventListener('click', () => {
        removeClass(this.elListWr, 'is-active');
        addClass(this.elInput, 'is-active');
        this.elInput.focus();
      });
      this.elInput.addEventListener('blur',() => {
        const newTitle = this.elInput.value;
        const matcher = this.options.matcher;
        const newItemIndex = this.list.findIndex(el => matcher(el.innerText, newTitle));
        if (!newTitle || !newItemIndex === -1) return;
        this.to(newItemIndex, null, 0);
        removeClass(this.elInput, 'is-active');
        addClass(this.elListWr, 'is-active');
      });
      this.elInput.addEventListener('change', this.elInput.blur);
    }
  }

  on(event, func) {
    this.elem.addEventListener('changer.' + event, () => func());
  }
}