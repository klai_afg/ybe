import { arrayByClass, domById } from 'utils/dom';

export default (anchor, params) => {
  
  let targetName = anchor.getAttribute('data-target') || anchor.getAttribute('href');
  
  if (targetName.length > 1 && targetName[0] === '#') {
    targetName = targetName.slice(1);
  }
  
  const targetDom = domById(targetName);
  
  if (targetDom === undefined) {
    return false;
  }
  
  const $scrollInstance = $('html, body');
  
  return () => {
  
    if (history.pushState) {
      history.pushState(null, null, '#' + targetName);
    }
    else {
      location.hash = '#' + targetName;
    }
  
    $scrollInstance.animate({
      scrollTop: targetDom.offsetTop - 20
    });
  }
  
}

