import { arrayByClass, addClass, removeClass } from 'utils/dom';

export class Eventable {
  constructor (instance, params) {
    
    if (instance.eventable) {
      return instance.eventable;
    }
    
    const defaultOptions = {
      mouseEnterClass: 'js-eventable-mouseenter',
      onMouseEnter (instance) {
        addClass(instance, 'is-mouseenter');
      },
      onMouseLeave (instance) {
        removeClass(instance, 'is-mouseenter');
      }
    };
    
    const options = Object.assign({}, defaultOptions, params);
    
    this.instance = instance;
    this.options = options;
    
    instance.eventable = this;
    
    this.init();
  }
  
  mouseEnterInit () {
    const {instance, options} = this;
    this._mouseEnterTargets = arrayByClass(instance, options.mouseEnterClass);
    this.mouseEnterEnable();
  }
  
  mouseEnterEnable () {
    const {_mouseEnterTargets} = this;
    
    const mouseEnterListener = e => this.options.onMouseEnter(this.instance);
    const mouseLeaveListener = e => this.options.onMouseLeave(this.instance);
    
    _mouseEnterTargets.forEach(target => {
      target.addEventListener('mouseenter', mouseEnterListener);
      target.addEventListener('mouseleave', mouseLeaveListener);
    });
  }
  
  init () {
    this.mouseEnterInit();
  }
}