import { arrayByClass, removeClass, addClass } from 'utils/dom';

const eventFrameReady = new CustomEvent('frameReady');

let frameReady = false;

export default class YoutubePlayer {

  constructor(el) {
    if(el.weberryYoutubePlayer) {
      return this;
    }

    this.el = el;

    const id = el.getAttribute('id');
    const url = el.getAttribute('data-url');

    if(id === undefined || !url) {
      console.log('player creating failed: id or url not defined');
      return;
    }

    this.id = id;
    this.url = url;
    this.instance = null;

    this._innerCreated = false;

    this.el.weberryYoutubePlayer = this;

    this.create();
  }



  create() {

    this._loadApi().then(() => {

      if(!this._innerCreated) {
        this._createInner();
      }

      this.instance = new YT.Player(this.id + this.url, {
        height: '100%',
        width: '100%',
        videoId: this.url,
        events: {
          'onReady': () => {
            this.el.dispatchEvent(eventFrameReady);
            this._handlerFrameReady();
          },
        }
      });

    })
  }

  setReady() {
    addClass(this.el, 'is-ready')
  }

  resetReady() {
    removeClass(this.el, 'is-ready')
  }

  _handlerFrameReady() {
    this.setReady();
  }

  _createInner() {
    const el = document.createElement('div');
    el.setAttribute('id', this.id + this.url);
    this.el.appendChild(el);
  }

  _loadApi() {
    return new Promise(resolve => {
      if(!frameReady) {
        const tag = document.createElement('script');
        tag.src = "https://www.youtube.com/iframe_api";
        const firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

        window.onYouTubeIframeAPIReady = () => {
          frameReady = true;
          resolve();
        };
      } else {
        resolve()
      }
    });

  }
};