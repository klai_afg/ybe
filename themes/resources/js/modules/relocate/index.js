import breakpoints from 'utils/breakpoints'
import {calcWindowWidth } from 'utils'
import {arrayByClass} from 'utils/dom'

export default (relocateInstance) => {
  
  const xsTarget = relocateInstance.getAttribute('data-target')
  const xlTarget = relocateInstance.getAttribute('data-target-xl')
  
  const targetBps = {
    xs: xsTarget,
    xl: xlTarget
  }
  
  const windowWidth = calcWindowWidth()
  
  let domTarget
  
  for (let breakpoint in breakpoints) {
    
    for (let targetBp in targetBps) {
      
      if (breakpoint === targetBp && windowWidth >= breakpoints[breakpoint]) {
        
        domTarget = arrayByClass(document, targetBps[targetBp], true)
        
      }
    }
    
  }
  
  if (domTarget !== undefined || relocateInstance.parentNode !== domTarget) {
    domTarget.appendChild(relocateInstance)
  }
};