export const addClass = (elem, classname) => {
  if(classname instanceof Array) {
    addClassList(elem, classname);
  } else if (classname) {
    if (elem.classList) elem.classList.add(classname);
    else elem.className += ' ' + classname;
  }

  return elem;
};

const addClassList = (elem, classnames) => classnames.forEach(classname => {addClass(elem, classname)});

export const removeClass = (elem, classname) => {
  if (classname) {
    if (elem.classList) elem.classList.remove(classname);
    else elem.className = elem.className.replace(new RegExp('(^|\\b)' + classname.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
  }
};

export const whichTransitionEvent = () => {
  let t,
    el = document.createElement('fakeelement');

  const animations = {
    'transition': 'transitionend',
    'OTransition': 'oTransitionEnd',
    'MozTransition': 'transitionend',
    'WebkitTransition': 'webkitTransitionEnd'
  };

  for (t in animations) {
    if (el.style[t] !== undefined) {
      return animations[t];
    }
  }
};

const transitionEvent = whichTransitionEvent();

export const transitionAnimate = (el, params) => {
  const animation = params.animation;
  const onComplete = params.onComplete || (() => {});
  let timeout;

  const _self = this;

  const transitionEndFn = (e) => {
    if(e.target !== el) {return}
    e.stopPropagation();
    el.removeEventListener(transitionEvent, transitionEndFn);
    onComplete();
  };

  el.addEventListener(transitionEvent, transitionEndFn, false);

  timeout = setTimeout(animation, 50);

  return {
    destroy() {
      clearTimeout(timeout);
      _self.removeEventListener(transitionEvent, transitionEndFn);
    }
  }
};