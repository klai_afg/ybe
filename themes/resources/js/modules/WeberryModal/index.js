import { addClass, removeClass, transitionAnimate} from './utils';

import WeberryModalOverlay from './overlay';
import { arrayByClass } from 'utils/dom';

const defaultOptions = {
  openingClass: 'is-opening',
  closingClass: 'is-closing',
  openedClass: 'is-opened',
  toggleCloseClass: 'js-modal-close',
  theme: null,
  overlayOptions: {},
};

const eventShow = new CustomEvent('wbry.modal.show');
const eventShown = new CustomEvent('wbry.modal.shown');
const eventHidden = new CustomEvent('wbry.modal.hidden');

class WeberryModal {
  constructor(id, params) {
    const elem = document.getElementById(id);

    if(!elem) {return}

    if (elem.WeberryModal) {
      return elem.WeberryModal;
    }

    const options = {...defaultOptions, ...params};

    this.name = id;
    this.elem = elem;
    this.options = options;

    this.isOpen = false;

    this.showInProgress = false;
    this.hideInProgress = false;

    this.closeHandler = () => this.close();

    const _self = this;

    if(options.theme) {
      addClass(elem, options.theme.split(' '));
    }

    this.overlay = new WeberryModalOverlay({
      onClick() {
        _self.close();
      },
      ...options.overlayOptions
    });

    this.registerToggleClose();

    elem.WeberryModal = this;
  }

  registerToggleClose() {
    const {elem, options: {toggleCloseClass}} = this;

    const toggleElems = arrayByClass(elem, toggleCloseClass);

    toggleElems.forEach(elem => elem.addEventListener('click', this.closeHandler));
  }

  toggle() {
    this.isOpen ? this.close() : this.open();
  }

  open() {
    const {overlay, elem} = this;

    const {showInProgress, hideInProgress, isOpen} = this;

    if(isOpen || showInProgress || hideInProgress) {
      return;
    }

    overlay.show(elem, () => {
      this.show();
    });
  }

  close() {
    const {overlay, elem} = this;

    const {showInProgress, hideInProgress, isOpen} = this;

    if(!isOpen || showInProgress || hideInProgress) {
      return;
    }

    this.hide(() => {
      overlay.hide(elem)
    });
  }

  show() {
    this.isOpen = true;

    this.showInProgress = true;

    this.setNoscrollBody();

    this.animateShow()
  }

  hide(hiddenCallback = () => {}) {
    this.isOpen = false;

    this.hideInProgress = true;

    this.unsetNoscrollBody();

    this.animateHide(hiddenCallback);
  }

  setNoscrollBody() {
    addClass(document.body, 'no-scroll');
  }
  unsetNoscrollBody() {
    removeClass(document.body, 'no-scroll');
  }

  get state() {
    return {
      showInprogress: this.showInProgress,
      hideInProgress: this.hideInProgress,
      isOpen: this.isOpen
    }
  }
}


//---------------------------------ANIMATE FUNCTIONS ------------------------------------

WeberryModal.prototype.animateShow = function () {
  const {elem, options} = this;

  const _self = this;

  elem.style.display = 'block';

  elem.dispatchEvent(eventShow);

  transitionAnimate(elem, {
    animation() {
      // console.log('OPEN ANIMATION IN PROGRESS');
      addClass(elem, options['openingClass']);
      elem.style.display = '';
    },
    onComplete() {
      // console.log('SHOW TRANSITION FN MODAL');

      removeClass(elem, options['openingClass']);
      addClass(elem, options['openedClass']);

      _self.showInProgress = false;

      elem.dispatchEvent(eventShown);
    }
  });
};

WeberryModal.prototype.animateHide = function (hiddenCallback) {
  const {elem, options} = this;

  const _self = this;

  transitionAnimate(elem, {
    animation() {
      // console.log('HIDE ANIMATION IN PROGRESS');
      removeClass(elem, options['openedClass']);
      addClass(elem, options['closingClass']);
    },
    onComplete() {
      // console.log('HIDE TRANSITION FN MODAL');

      removeClass(elem, options['closingClass']);
      _self.hideInProgress = false;

      elem.dispatchEvent(eventHidden);
      hiddenCallback();
    }
  });
};

export default WeberryModal;