import { addClass, removeClass, transitionAnimate } from './utils';

const defaultOptions = {
  mainClass: 'modal-overlay',
  theme: null,
  openingClass: 'is-opening',
  openedClass: 'is-opened',
  primaryClass: 'is-primary',
  closingClass: 'is-closing',
  onClick() {}
};

class WeberryModalOverlay {
  constructor(params) {

    if(this.elem && this.elem.WeberryModalOverlay) {
      return
    }

    this.options = {...defaultOptions, ...params};

    this.isOpen = false;

    this.showInProgress = false;
    this.hideInProgress = false;

  }

  show(modalElem, shownCallback = () => {}) {
    const {showInProgress, hideInProgress, isOpen} = this;


    if(isOpen || showInProgress || hideInProgress) {
      return;
    }

    this.isOpen = true;

    this.showInProgress = true;

    this.create();

    this.definePrimaryOverlay();

    this.animateShow(() => {
      this.addModal(modalElem);

      shownCallback();
    });
  }

  add() {
    document.body.appendChild(this.elem);
  }

  remove() {
    document.body.removeChild(this.elem);
  }

  addModal(modalElem) {
    this.elem.appendChild(modalElem);
  }

  removeModal(modalElem) {
    document.body.appendChild(modalElem);
  }

  hide(modalElem, hiddenCallback = () => {}) {
    const {showInProgress, hideInProgress, isOpen} = this;

    if(!isOpen || showInProgress || hideInProgress) {
      return;
    }

    this.isOpen = false;

    this.hideInProgress = true;

    this.animateHide(() => {
      this.removeModal(modalElem);
      this.remove();
      this.definePrimaryOverlay();
      hiddenCallback();
    });
  }

  create() {
    const {options} = this;

    const {mainClass, theme} = this.options;

    const elem = document.createElement("div");

    addClass(elem, mainClass);

    if(theme) {
      addClass(elem, theme);
    }

    this.elem = elem;

    this.add();

    elem.addEventListener('click', (e) => {

      if(e.target === elem) {
        options.onClick();
      }

    });

    elem.WeberryModalOverlay = this;
  }

  definePrimaryOverlay() {
    const {mainClass, primaryClass} = this.options;

    const overlaysDom = document.querySelectorAll(`.${mainClass}`);
    const overlaysDomLastIndex = overlaysDom.length - 1;

    overlaysDom.forEach((overlayDom, index) => {

      if(index === overlaysDomLastIndex) {
        addClass(overlayDom, primaryClass);
      } else {
        removeClass(overlayDom, primaryClass);
      }
    });
  }
}

//---------------------------------ANIMATE FUNCTIONS ------------------------------------

WeberryModalOverlay.prototype.animateShow = function (onComplete = () => {}) {
  const {elem, options} = this;

  const _self = this;

  elem.style.display = 'block';

  transitionAnimate(elem, {
    animation() {
      elem.style.display = '';
      addClass(elem, options['openingClass']);
    },
    onComplete() {
      // console.log('SHOW TRANSITION FN OVERLAY');

      removeClass(elem, options['openingClass']);
      addClass(elem, options['openedClass']);

      _self.showInProgress = false;

      onComplete();
    }
  });
};

WeberryModalOverlay.prototype.animateHide = function (hiddenCallback) {
  const {elem, options} = this;

  const _self = this;

  transitionAnimate(elem, {
    animation() {
      removeClass(elem, options['openedClass']);
      addClass(elem, options['closingClass']);
    },
    onComplete() {
      // console.log('HIDE TRANSITION FN OVERLAY');

      removeClass(elem, options['closingClass']);

      _self.hideInProgress = false;

      hiddenCallback()
    }
  });
};

export default WeberryModalOverlay;