import { Selectaps } from "modules/selectaps";
import { arrayByClass } from "utils/dom";

arrayByClass(document, 'js-select')
  .forEach(selectContainer => {
    const select = selectContainer.querySelector('select');
    select.selectedIndex = -1;
    const selectTitle = select.getAttribute('data-title');
    selectContainer.selectaps = new Selectaps(selectContainer);
  });