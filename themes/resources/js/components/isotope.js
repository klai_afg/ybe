import {arrayByClass} from "utils/dom";
import Isotope from 'isotope-layout/dist/isotope.pkgd.min.js';
import {calcWindowWidth} from "utils";
import breakpoints from "utils/breakpoints";

const isotopeFilterFunctions = {
  gender: (itemElem, val) => itemElem.getAttribute('data-gender') === val,
  age: (itemElem, val) => {
    const itemAgeRange = itemElem.getAttribute('data-age');
    if (!itemAgeRange) return false;
    const itemAgeRangeArray = itemAgeRange.split('-');
    const itemAgeFrom = +itemAgeRangeArray[0];
    const itemAgeTo = +itemAgeRangeArray[1];
    const filterAgeRangeArray = val.split('-');
    const filterAgeFrom = +filterAgeRangeArray[0];
    const filterAgeTo = +filterAgeRangeArray[1];
    return itemAgeFrom <= filterAgeFrom && filterAgeTo <= itemAgeTo;
  },
  theme: (itemElem, val) => {
    return itemElem.getAttribute('data-theme') === val;
  }
};

const elGamesList = document.querySelector('.js-games-list');

if (elGamesList && calcWindowWidth() >= breakpoints['md']) {
  const iso = new Isotope( elGamesList, {
    itemSelector: '.js-games-item',
    layoutMode: 'fitRows'
  });

  const gamesFilterWr = document.querySelector('.js-games-filter');
  if (gamesFilterWr) {

    const filterSelects = arrayByClass(gamesFilterWr, 'js-select');
    filterSelects.forEach(selectContainer => {
      selectContainer.selectaps && selectContainer.selectaps.on('select', val => {
        iso.arrange({
          // item element provided as argument
          filter: function( i, itemElem ) {
            return isotopeFilterFunctions[selectContainer.getAttribute('data-field')](itemElem, val);
          }
        });
      })
    });

    const gamesFilterClear = gamesFilterWr.querySelector('.js-games-filter-clear');
    gamesFilterClear.addEventListener('click', function() {
      iso.arrange({
        filter: function() {return true;}
      })
    });
  }
}