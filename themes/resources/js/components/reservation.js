import {addClass, arrayByClass, removeClass} from "utils/dom";

const reservationProgrammeType = document.querySelector('.js-reservation-programme-type');
reservationProgrammeType && arrayByClass(reservationProgrammeType, 'js-form-elems-list-item-input').forEach(checkField => {
  checkField.addEventListener('change', function() {
    arrayByClass(document, 'js-reservation-programme-choice').forEach(el => {
      addClass(el, 'd-none');
      el.selectaps.close();
      el.selectaps.clear();
    });
    const currentFormSelect = document.querySelector('.js-reservation-programme-choice.js-reservation-' + this.value + '-choice');

    currentFormSelect && removeClass(currentFormSelect, 'd-none');
  });
});