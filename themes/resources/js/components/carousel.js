import {svg} from "utils/svg";
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel';
import {calcWindowWidth} from "utils";
import breakpoints from "utils/breakpoints";

$('.js-shows-carousel').owlCarousel({
  dots: true,
  loop: true,
  nav: true,
  navText: ['<span class="owl-prev__inner"><span class="owl-prev__content">' +
  svg({name: 'arrow_small', mod: 'owl-prev__svg'})+
  '</span><span class="owl-prev__bg"></span></span>','<span class="owl-next__inner"><span class="owl-next__content">' +
  svg({name: 'arrow_small', mod: 'owl-next__svg'})+
  '</span><span class="owl-next__bg"></span></span>'],
  items: 1,
  dotsEach: true,
  dotsData: true
});

const owls = $('.js-extra-carousel,.js-photos-carousel');

owls.owlCarousel({
  loop: true,
  nav: true,
  smartSpeed: 1000,
  navText: ['<span class="owl-prev__inner"><span class="owl-prev__content">' +
  svg({name: 'arrow_small', mod: 'owl-prev__svg'})+
  '</span><span class="owl-prev__bg"></span></span>','<span class="owl-next__inner"><span class="owl-next__content">' +
  svg({name: 'arrow_small', mod: 'owl-next__svg'})+
  '</span><span class="owl-next__bg"></span></span>'],
  responsive: {
    [breakpoints['xxs']]: {
      items: 1,
      dots: true,
      dotsEach: true,
      dotsData: true
    },
    [breakpoints['md']]: {
      center: true,
      dots: false,
      items: 1
    },
    [breakpoints['xl']] : {
      items: 3,
      center: true,
      dots: false
    }
  }
});

const $mobileCarousels = $('.js-why-mobile-carousel,.js-games-mobile-carousel,.js-testimonials-mobile-carousel,.js-other-games-mobile-carousel,.js-other-shows-mobile-carousel');

if (calcWindowWidth() < breakpoints['md']) {
  $mobileCarousels.addClass('owl-carousel');
  $mobileCarousels.owlCarousel({
    loop: true,
    nav: true,
    smartSpeed: 1000,
    dots: true,
    dotsEach: true,
    dotsData: true,
    items: 1,
    navText: ['<span class="owl-prev__inner"><span class="owl-prev__content">' +
    svg({name: 'arrow_small', mod: 'owl-prev__svg'})+
    '</span><span class="owl-prev__bg"></span></span>','<span class="owl-next__inner"><span class="owl-next__content">' +
    svg({name: 'arrow_small', mod: 'owl-next__svg'})+
    '</span><span class="owl-next__bg"></span></span>']
  });
} else {
  $mobileCarousels.removeClass('owl-carousel');
}

