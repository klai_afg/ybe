import {arrayByClass} from "utils/dom";
import YoutubePlayer from "modules/youtube-api";
import anime from "animejs";
import {calcWindowHeight, calcWindowWidth} from "utils";

const videos = arrayByClass(document, 'js-video');
videos.forEach(container => {
  const content = container.querySelector('.js-video-content');
  const button = container.querySelector('.js-video-button');
  const visualButton = container.querySelector('.js-video-visual-button');
  const player = container.querySelector('.js-video-player');
  const overlay = container.querySelector('.js-video-overlay');
  new YoutubePlayer(player);

  button.addEventListener('click', function() {
    anime({
      targets: content,
      width: calcWindowHeight(),
      height: calcWindowWidth(),
      translateY: pageYOffset,
      duration: 1000,
      easing: 'easeInQuad',
      complete: function() {
        player.style.width = '100%';
        player.style.height = '100%';

        anime({
          targets: overlay,
          opacity: '0',
          duration: 500,
          complete: function() {
            overlay.style.display = 'none';
          }
        });
      }
    });
    anime({
      targets: visualButton,
      opacity: '0',
      duration: 500
    })
  })
});