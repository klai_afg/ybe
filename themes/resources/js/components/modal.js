import WeberryModal from "modules/WeberryModal";

const modalFeedback = new WeberryModal('modalFeedback', {
  overlayOptions: {
    mainClass: 'modal-main-overlay'
  }
});
const feedbackToggle = document.querySelector('.js-feedback-toggle');
feedbackToggle.addEventListener('click', function() {
  modalFeedback.toggle();
});
modalFeedback.elem.addEventListener('wbry.modal.show', () => {
  feedbackToggle.setAttribute('data-open', 'true');
});
modalFeedback.elem.addEventListener('wbry.modal.hidden', () => {
  feedbackToggle.setAttribute('data-open', 'false');
});
