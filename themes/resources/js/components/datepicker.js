import {addClass, removeClass} from "utils/dom";

$('.js-form-datepicker').datepicker({
  orientation: 'auto bottom',
  templates: {
    leftArrow: '<span class="form-datepicker__left"></span>',
    rightArrow: '<span class="form-datepicker__right"></span>'
  },
  format: 'mm-dd-yyyy'
})
  .on('show', function(e) {
    addClass(e.target, 'is-focused');
  })
  .on('hide', function(e) {
    removeClass(e.target, 'is-focused');
  })
  .on('clearDate', function(e) {
    addClass(e.target, 'is-empty');
  })
  .on('changeDate', function(e) {
    removeClass(e.target, 'is-empty');
  });