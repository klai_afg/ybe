import {addClass, arrayByClass, removeClass} from "utils/dom";

arrayByClass(document, 'js-form-text')
  .forEach(formInputLabel => {
    const formInput = formInputLabel.querySelector('input, textarea');
    formInput.addEventListener('focus', function() {
      addClass(formInputLabel, 'is-focused');
    });
    formInput.addEventListener('blur', function() {
      removeClass(formInputLabel, 'is-focused');
      if (this.value === '') addClass(formInputLabel, 'is-empty'); else removeClass(formInputLabel, 'is-empty');
    })
  });