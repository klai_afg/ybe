import './_app-common';

import 'plugins/jQuery';

import 'bootstrap-datepicker';

import 'modules/browserDetect';

import '../sass/main.scss';

import {calcWindowHeight, calcWindowWidth, onReady} from 'utils';
import { arrayByClass, addClass, removeClass } from "utils/dom";
import {Changer} from "modules/changer";
import breakpoints from "utils/breakpoints";
import WeberryCollapse from 'weberry-collapse';

{
  onReady(() => {

    const windowHeight = calcWindowHeight();
    const windowWidth = calcWindowWidth();

    arrayByClass(document, 'js-height-window')
      .forEach(div => {
        if (windowHeight > 500 && windowWidth < breakpoints['md']) {
          div.style.height = 452 + 'px';
        } else {
          div.style.height = windowHeight + 'px';
        }
      });

    require('components/carousel');
    require('components/select');
    require('components/text');
    require('components/datepicker');
    require('components/video');
    require('components/isotope');
    require('components/reservation');
    require('components/modal');

    arrayByClass(document, 'js-form-changer').forEach(changerContainer => new Changer(changerContainer));
    
    if (windowWidth < breakpoints['xl']) {
      const elSandwichCollapse = document.querySelector('.js-sandwich-collapse');
      const sandwichCollapse = new WeberryCollapse(elSandwichCollapse, {
        calcHeightShowFn: calcWindowHeight,
        clearHeightShown: false
      });
      elSandwichCollapse.addEventListener('wbry.collapse.show', () => {
        addClass(document.querySelector('.js-navbar'), 'is-sandwich-expanded');
        addClass(document.body, 'no-scroll');
      });
      elSandwichCollapse.addEventListener('wbry.collapse.hide', () => {
        removeClass(document.querySelector('.js-navbar'), 'is-sandwich-expanded');
        removeClass(document.body, 'no-scroll');
      });
    }

  });
}