export const svg = (params) => {
  
  
  const dirName = params.dir ? params.dir + '--' : '';
  const multi = params.type === 'multi';
  const prefix = multi ? '' : 's-';
  
  const iconName = dirName + prefix + params.name;
  
  const className = `icon-svg_${iconName} ${multi ? '' : 'icon-svg_s'}`;
  
  const pathName = '/themes/resources/assets/images/sprite.svg#' + iconName;
  
  return `<svg class="icon-svg ${className} ${params.mod ? params.mod : ''}">` +
    `<use xlink:href="${pathName}"></use>` +
    `</svg>`
};
