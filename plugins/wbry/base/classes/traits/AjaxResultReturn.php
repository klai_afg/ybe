<?php

namespace Wbry\Base\Classes\Traits;

/**
 * Ajax return result data
 *
 * @package Wbry\Base\Classes\Traits
 * @author Diamond Systems
 */
trait AjaxResultReturn
{
    /**
     * Return success result data
     *
     * @param string $msg   - result message
     * @param array  $data  - result data
     * @param array  $other - other result data
     *
     * @return array
     */
    public function resultSuccess(string $msg = NULL, array $data = [], array $other = [])
    {
        return $this->resultResult($msg, $data, 'success', $other);
    }

    /**
     * Return error result data
     *
     * @param string $msg   - result message
     * @param array  $data  - result data
     * @param array  $other - other result data
     *
     * @return array
     */
    public function resultError(string $msg = '', array $data = [], array $other = [])
    {
        return $this->resultResult($msg, $data, 'error', $other);
    }

    /**
     * Return result data
     *
     * @param array  $data  - result data
     * @param array  $other - other result data
     *
     * @return array
     */
    public function resultData(array $data = [], array $other = [])
    {
        return $this->resultResult('', $data, 'data', $other);
    }

    /**
     * Return success result data
     *
     * @param string $msg    - result message
     * @param array  $data   - result data
     * @param string $status - 'success' or 'error' or either your option
     * @param array  $other  - other result data
     *
     * @return array
     */
    public function resultResult(string $msg = '', array $data = [], string $status = 'success', array $other = [])
    {
        return array_merge([
            'status' => $status,
            'msg'    => $msg,
            'data'   => $data,
        ], $other);
    }
}