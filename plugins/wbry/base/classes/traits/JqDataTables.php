<?php

namespace Wbry\Base\Classes\Traits;

use DB;
use Input;
use Event;
use Model;
use ApplicationException;
use October\Rain\Database\Builder;

/**
 * Trait JqDataTables
 *
 * @package Wbry\AppReg\Classes\Traits
 * @author Diamond Systems
 * @version 1.0.4
 */
trait JqDataTables
{
    public static $isQueryLog = false;

    /**
     * Get array filtered data result jQuery Data Tables
     *
     * @param Model $model
     * @param array $columns - look at the model 'filterColumnsStructure()'
     *
     * @return array
     */
    public function resultJqDataTable(Model $model, array $columns)
    {
        return $this->resultJqDataTableQuery($model, $model::select(), $columns);
    }

    /**
     * Get array filtered data result jQuery Data Tables
     *
     * @param Model $model
     * @param Builder $query
     * @param array $columns - look at the model 'filterColumnsStructure()'
     * @param array $modelScopes
     *
     * @return array
     */
    public function resultJqDataTableQuery(Model $model, Builder $query, array $columns, array $modelScopes = [])
    {
        $columns  = $this->filterColumnsStructure($columns);
        $isSearch = $this->querySearchFiltered($query, $columns);
        $isOrder  = $this->queryOrderFiltered($query, $columns);

        return $this->resultFilteredData($model, $query, $columns, $isSearch, $modelScopes);
    }

    /**
     * Filter columns structure
     *
     * @param $columns
     *    $columns = [
     *      'asColumnName1' => [
     *          'column'     => (string) (required) Table column name
     *          'typeString' => (bool)   (optional) If the string type column, set value true or false. Default: true
     *          'relation'   => (string) (optional) Relation name. Default: ''
     *          'search'     => (string) (optional) Search string. Default: ''
     *          'order'      => (string) (optional) OrderBy type ('asc' or 'desc'). Default: ''
     *      ],
     *      etc...
     *    ]
     * @return array
     */
    public function filterColumnsStructure($columns)
    {
        $this->enableQueryLog();

        if (empty($columns))
            throw new ApplicationException('Columns not found');

        /*
         * Inputs
         */
        $inputs    = Input::only('columns', 'order', 'search');
        $inpSearch = (! empty($inputs['search']) && ! empty($inputs['search']['value'])) ? trim($inputs['search']['value']) : '';

        $inputColumns = [];
        $inputOrders  = [];

        if (! empty($inputs['order']) && is_array($inputs['order']) && count($inputs['order']) < 101)
        {
            foreach ($inputs['order'] as $inpOrder)
            {
                if (isset($inpOrder['column'], $inpOrder['dir']) && is_numeric($inpOrder['column']) && preg_match("/^(asc|desc)$/i", trim($inpOrder['dir'])))
                    $inputOrders[(int)$inpOrder['column']] = trim($inpOrder['dir']);
            }
        }

        if (! empty($inputs['columns']) && is_array($inputs['columns']) && count($inputs['columns']) < 101)
        {
            foreach ($inputs['columns'] as $inpColumnK => $inpColumn)
            {
                if (empty($inpColumn['data']))
                    continue;

                $inputColumns[$inpColumn['data']] = [
                    'searchable' => ($inpSearch && preg_match("/^(true|1)$/i", $inpColumn['searchable'])) ? $inpSearch : '',
                    'orderable'  => (isset($inputOrders[$inpColumnK]) && preg_match("/^(true|1)$/i", $inpColumn['orderable'])) ? $inputOrders[$inpColumnK] : false
                ];
            }
        }

        unset($inputs, $inputOrders, $inpOrder, $inpColumnK, $inpColumn);

        /*
         * Filters
         */
        $resColumns = [];
        foreach ($columns as $key => $column)
        {
            $newColumn = [];

            # column
            if (empty($column))
                throw new ApplicationException('Empty AS column "'.$key.'"');

            if (empty($column['column']) || ! is_string($column['column']))
                throw new ApplicationException('Empty column name for "'.$key.'"');

            $newColumn['column'] = $column['column'];

            # searchable
            if (! isset($column['typeString']) || (bool)$column['typeString'])
            {
                if (! isset($column['search']) || ! is_string($column['search']))
                    $newColumn['search'] = isset($inputColumns[$key]) ? $inputColumns[$key]['searchable'] : '';
            }
            $newColumn['search'] = $newColumn['search'] ?? '';

            # order
            if (! isset($column['order']) || ! is_string($column['order']) || ! preg_match("/^(asc|desc)$/i", $column['order']))
                $newColumn['order'] = isset($inputColumns[$key]) ? $inputColumns[$key]['orderable'] : '';
            else
                $newColumn['order'] = $column['order'];

            # relation
            $newColumn['relation'] = (! isset($column['relation']) || ! is_string($column['relation'])) ? '' : $column['relation'];

            # unset
            unset($inputColumns[$key]);

            $resColumns[$key] = $newColumn;
        }

        return $resColumns;
    }

    /**
     * Query search filtered
     *
     * @param Builder $query
     * @param array $columns
     *
     * @return bool
     */
    public function querySearchFiltered(Builder $query, array $columns)
    {
        $isSearch = false;
        foreach ($columns as $col)
        {
            if ($col['search'])
            {
                if (! $isSearch)
                    $isSearch = true;

                if ($col['relation'])
                {
                    $query->orWhereHas($col['relation'], function ($query) use ($col) {
                        $query->where($col['column'], 'like', '%'.mb_strtolower($col['search']).'%');
                    });
                }
                else
                    $query->orWhere($col['column'], 'like', '%'.mb_strtolower($col['search']).'%');
            }
        }

        return $isSearch;
    }

    /**
     * Query order filtered
     *
     * @param Builder $query
     * @param array $columns
     *
     * @return bool
     */
    public function queryOrderFiltered(Builder $query, array $columns)
    {
        $isOrder = false;
        foreach ($columns as $col)
        {
            if ($col['order'])
            {
                if (! $isOrder)
                    $isOrder = true;

                // TODO realization relation order by
                // variant: $users = User::join('roles', 'users.role_id', '=', 'roles.id')->orderBy('roles.label', $order)->select('users.*')->paginate(10);
//                if ($col['relation'])
//                {}
//                else
                $query->orderBy($col['column'], $col['order']);
            }
        }

        return $isOrder;
    }

    /**
     * Result filtered data
     *
     * @param Model $model
     * @param Builder $query
     * @param array $columns  - look at the model 'filterColumnsStructure()'
     * @param bool $isRecordsFiltered
     * @param array $modelScopes
     *
     * @return array
     */
    public function resultFilteredData(Model $model, Builder $query, array $columns, bool $isRecordsFiltered = true, array $modelScopes = [])
    {
        # Total record
        if ($modelScopes)
        {
            $recAllCnt = $model::make();
            foreach ($modelScopes as $mScope)
                $recAllCnt = $recAllCnt->{$mScope}();
            $recAllCnt = $recAllCnt->count();
        }
        else
            $recAllCnt = $model::count();

        # Configs
        $returnData = [
            'draw'            => request('draw', 0),
            'recordsTotal'    => $recAllCnt,
            'recordsFiltered' => $isRecordsFiltered ? $query->count() : $recAllCnt,
        ];

        # Limit
        $start = (int)request('start', 0);
        $query->skip(($start < 0) ? 0 : $start);

        $length = (int)request('length', 0);
        $query->take(($length < 1) ? 1 : $length);

        # result data
        $resData  = [];
        $resQuery = $query->get();

        foreach ($resQuery as $rec)
        {
            $itemRec = [];
            foreach ($columns as $colK => $colV)
            {
                if ($colV['relation'])
                    $itemRec[$colK] = $rec->{$colV['relation']}->{$colV['column']};
                else
                    $itemRec[$colK] = $rec->{$colV['column']};
            }
            $resData[] = $itemRec;
        }
        $returnData['data'] = &$resData;

        $this->getQueryLog();

        return $returnData;
    }

    /*
     * Helpers
     */

    private function enableQueryLog()
    {
        if (self::$isQueryLog)
            DB::enableQueryLog();
    }

    private function getQueryLog()
    {
        if (self::$isQueryLog)
            dd(DB::getQueryLog());
    }
}