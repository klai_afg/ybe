<?php

namespace Wbry\Base\Classes;

use October\Rain\Support\Traits\Singleton;
use System\Classes\PluginManager;

/**
 * Search classes - models, controllers, etc.
 *
 * @package wbry\base
 * @author Diamond Systems
 */
class SearchClasses
{
    use Singleton;

    /**
     * Cache modal list
     * @var null/array
     */
    protected $modelList = null;

    /**
     * Cache controller list
     * @var null/array
     */
    protected $controllerList = null;

    /**
     * Get global plugins model list
     *
     * @return array
     */
    public function listGlobalModels()
    {
        if ($this->modelList !== null)
            return $this->modelList;

        return $this->modelList = $this->searchPluginClasses('models', 'October\Rain\Database\Model');
    }

    /**
     * Get global plugins controller list
     *
     * @return array
     */
    public function listGlobalControllers()
    {
        if ($this->controllerList !== null)
            return $this->controllerList;

        return $this->controllerList = $this->searchPluginClasses('controllers', 'Backend\Classes\Controller');
    }

    /**
     * Search plugin classes - models, controllers etc.
     *
     * @param string $dir              - dir name - models or controllers etc.
     * @param string $parentClassName  - (optional) Parent class name
     *
     * @return array
     */
    public function searchPluginClasses($dir, $parentClassName = '')
    {
        $dirUcF = ucfirst($dir);
        $result = [];
        $pluginManager = PluginManager::instance();

        foreach (array_keys($pluginManager->getPlugins()) as $pluginId)
        {
            $codeParts = explode('.', $pluginId);
            if (count($codeParts) !== 2)
                continue;

            $classesPath = $pluginManager->getPluginPath($pluginId).'/'.$dir;
            if (! file_exists($classesPath))
                continue;

            foreach (new \DirectoryIterator($classesPath) as $fileInfo)
            {
                if (! $fileInfo->isFile() || $fileInfo->getExtension() != 'php')
                    continue;

                $fileBaseName = $fileInfo->getBasename('.php');
                $className = $codeParts[0].'\\'.$codeParts[1].'\\'. $dirUcF .'\\'.$fileBaseName;

                if (class_exists($className))
                {
                    if ($parentClassName)
                    {
                        $isModel     = false;
                        $classSearch = $className;
                        do {
                            if ($classSearch === $parentClassName)
                            {
                                $isModel = true;
                                break;
                            }
                        } while ($classSearch = get_parent_class($classSearch));
                    }
                    else
                        $isModel = true;

                    if ($isModel)
                        $result[$className] = $pluginId .' - '. $fileBaseName;
                }
            }
        }

        return $result;
    }
}
