<?php

namespace Wbry\Base;

use Backend;
use System\Classes\PluginBase;

/**
 * Base Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Base',
            'description' => 'No description provided yet...',
            'author'      => 'Wbry',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        $this->registerConsoleCommand('wbry.newuser', 'Wbry\Base\Console\CreateUser');
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {}

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            Components\Base::class => 'base',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'wbry.base.some_permission' => [
                'tab' => 'Base',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'base' => [
                'label'       => 'Base',
                'url'         => Backend::url('wbry/base/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['wbry.base.*'],
                'order'       => 500,
            ],
        ];
    }

    public function registerMarkupTags()
    {
        return [
            'filters' => [
                // link to images folder
                'assets' => [$this, 'assetsPathByName']
            ],

            'functions' => [
                // Svg element generator for svg sprite
                'svg_icon' => [$this, 'svg_icon']
            ]
        ];
    }

    public function svg_icon($params)
    {
        $path      = '/themes/resources/assets/images/sprite.svg#';
        $dir       = !empty($params['dir']) ? $params['dir'].'--':'';
        $multi     = !empty($params['multi']) ? $params['multi']: 'false';
        $prefix    = $multi === 'false' ? 's-' : '';
        $className = $multi === 'false' ? ' icon-svg_s' : '';
        $iconName  = $dir.$prefix.$params['name'];

        $rtrn =
            '<svg class="icon-svg'.$className.' icon-svg_'.$iconName.' '.$params['mod'].'">'
            . '   <use xlink:href="'.$path.$iconName.'"></use>'
            . '</svg>';

        return $rtrn;
    }

    public function assetsPathByName($filename)
    {
        return '/themes/resources/assets/'.$filename;
    }
}
