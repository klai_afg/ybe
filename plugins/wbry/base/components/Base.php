<?php

namespace Wbry\Base\Components;

use Cms\Classes\ComponentBase;

/**
 * Base Component
 *
 * @package Wbry\Base\Components
 * @author Diamond Systems
 */
class Base extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Базовый компонент',
            'description' => 'Компонет для обработки глобальных запросов'
        ];
    }
}
