<?php namespace Wbry\Base\Database\Seeds;

use Seeder;
use Backend\Models\UserRole;
use Backend\Models\UserGroup;


class UserRolesAndGroupsSeeder extends Seeder
{
    public function run()
    {
        if(!UserRole::where('code', '=', UserRole::CODE_PUBLISHER)->first()){
            UserRole::create([
                'name'              =>  'Publisher',
                'code'              =>  'publisher',
                'description'       =>  'Site editor with access to publishing tools.',
                'is_system'         =>  '1',
            ]);
        }

        if(!UserRole::where('code', '=', UserRole::CODE_DEVELOPER)->first()) {
            UserRole::create([
                'name' => 'Developer',
                'code' => 'developer',
                'description' => 'Site administrator with access to developer tools.',
                'is_system' => '1',
            ]);
        }

        if(!UserGroup::where('code','=', UserGroup::CODE_OWNERS)){
            UserGroup::create([
                'name' => 'Owners',
                'code' => UserGroup::CODE_OWNERS,
                'description' => 'Default group for website owners.',
                'is_new_user_default' => false
            ]);
        }
    }
}
