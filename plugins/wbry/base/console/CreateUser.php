<?php namespace Wbry\Base\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Wbry\Base\Database\Seeds\UserRolesAndGroupsSeeder;
use Backend\Models\User;
use Backend\Models\UserRole;
use Backend\Models\UserGroup;


use Db;

class CreateUser extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'wbry:newuser';

    /**
     * @var string The console command description.
     */
    protected $description = 'Create new user';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $firstname = $this->ask('Firstname ?');
        $lastname = $this->ask('Lastname ?');
        $login = $this->ask('Login ?');
        $email = $this->ask('Email ?');

        $password = $this->secret('Password ?');
        $password_confirm = $this->secret('Confirm Password ?');

        if($password!=$password_confirm){
            $this->output->writeln('Password confirm failed');
            return;
        }

        $is_superuser = $this->confirm('Is superuser? [yes|no]');
        $is_owner = $this->confirm('Is owner? [yes|no]');

        (new UserRolesAndGroupsSeeder)->run();

        $roles = DB::table('backend_user_roles')->pluck('name', 'id')->all();
        $rolequestion = "Choose user role: \n";
        foreach ($roles as $name => $id) {
            $rolequestion.='('.$id.') - '.$name."\n";
        }

        $user_role_id = $this->ask($rolequestion);

        if(!array_key_exists($user_role_id, $roles)){
            $this->output->writeln('Role id doesn\'t exist');
            return;
        }

        $user = User::create([
            'email'                 => $email,
            'login'                 => $login,
            'password'              => $password,
            'password_confirmation' => $password_confirm,
            'first_name'            => $firstname,
            'last_name'             => $lastname,
            'is_activated'          => true
        ]);

        if($user){
            $user->is_superuser = $is_superuser;

            $role = UserRole::find($user_role_id);
            $user->role()->associate($role);

            if($is_owner){
                $group = UserGroup::where('code', '=', UserGroup::CODE_OWNERS)->first();
                $user->addGroup($group);
            }

            $user->save();

            $this->info('Success');
        }else{
            $this->error('Error');
        }

    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

}